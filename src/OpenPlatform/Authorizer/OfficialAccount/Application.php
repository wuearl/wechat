<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\OpenPlatform\Authorizer\OfficialAccount;

use Wechat\OfficialAccount\Application as OfficialAccount;
use Wechat\OpenPlatform\Authorizer\Aggregate\AggregateServiceProvider;

/**
 * Class Application.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 *
 * @property \Wechat\OpenPlatform\Authorizer\OfficialAccount\Account\Client     $account
 * @property \Wechat\OpenPlatform\Authorizer\OfficialAccount\MiniProgram\Client $mini_program
 */
class Application extends OfficialAccount
{
    /**
     * Application constructor.
     *
     * @param array $config
     * @param array $prepends
     */
    public function __construct(array $config = [], array $prepends = [])
    {
        parent::__construct($config, $prepends);

        $providers = [
            AggregateServiceProvider::class,
            MiniProgram\ServiceProvider::class,
        ];

        foreach ($providers as $provider) {
            $this->register(new $provider());
        }
    }
}
