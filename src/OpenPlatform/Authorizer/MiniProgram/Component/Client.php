<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\OpenPlatform\Authorizer\MiniProgram\Component;

use Wechat\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author dudashuang <dudashuang1222@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 配置小程序用户隐私保护指引
     * @param $ownerSetting
     * @param null $setting_list
     * @param int $privacyVer
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function setPrivacy($ownerSetting, $setting_list = null, $privacyVer = 2)
    {
        $params = [
            'owner_setting' => $ownerSetting,
            'privacy_ver' => $privacyVer,
            'setting_list' => $setting_list,
        ];
        return $this->httpPostJson('cgi-bin/component/setprivacysetting', $params);
    }

    /**
     * 查询小程序用户隐私保护指引
     * @param int $privacyVer
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function getPrivacy($privacyVer = 2)
    {
        $params = [
            'privacy_ver' => $privacyVer
        ];
        return $this->httpPostJson('cgi-bin/component/getprivacysetting', $params);
    }

    /**
     * 上传小程序用户隐私保护指引
     * @param $path
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function uploadPrivacy($path)
    {
        return $this->httpUpload('cgi-bin/component/uploadprivacyextfile', ['file' => $path]);
    }
}
