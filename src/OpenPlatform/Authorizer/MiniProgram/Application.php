<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\OpenPlatform\Authorizer\MiniProgram;

use Wechat\MiniProgram\Application as MiniProgram;
use Wechat\OpenPlatform\Authorizer\Aggregate\AggregateServiceProvider;

/**
 * Class Application.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 *
 * @property \Wechat\OpenPlatform\Authorizer\MiniProgram\Account\Client   $account
 * @property \Wechat\OpenPlatform\Authorizer\MiniProgram\Code\Client      $code
 * @property \Wechat\OpenPlatform\Authorizer\MiniProgram\Domain\Client    $domain
 * @property \Wechat\OpenPlatform\Authorizer\MiniProgram\Setting\Client   $setting
 * @property \Wechat\OpenPlatform\Authorizer\MiniProgram\Tester\Client    $tester
 * @property \Wechat\OpenPlatform\Authorizer\MiniProgram\Component\Client $component
 */
class Application extends MiniProgram
{
    /**
     * Application constructor.
     *
     * @param array $config
     * @param array $prepends
     */
    public function __construct(array $config = [], array $prepends = [])
    {
        parent::__construct($config, $prepends);

        $providers = [
            AggregateServiceProvider::class,
            Code\ServiceProvider::class,
            Domain\ServiceProvider::class,
            Account\ServiceProvider::class,
            Setting\ServiceProvider::class,
            Tester\ServiceProvider::class,
            Component\ServiceProvider::class,
        ];

        foreach ($providers as $provider) {
            $this->register(new $provider());
        }
    }
}
