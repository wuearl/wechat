<?php

/*
 * This file is part of the overtrue/wechat.
 *
 */

namespace Wechat\MiniProgram\Business;

use HttpBase\Exceptions\InvalidArgumentException;
use Wechat\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author wangdongzhao <elim051@163.com>
 */
class Client extends BaseClient
{
    /**
     * Business register
     * @param string $accountName
     * @param string $nickname
     * @param string $iconMediaId
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function register(string $accountName, string $nickname, string $iconMediaId)
    {
        $params = [
            'account_name' => $accountName,
            'nickname' => $nickname,
            'icon_media_id' => $iconMediaId,
        ];

        return $this->httpPostJson('cgi-bin/business/register', $params);
    }

    /**
     * Get business
     * @param int $businessId
     * @param string $accountName
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws InvalidArgumentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function getBusiness(int $businessId = 0, string $accountName = '')
    {
        if (empty($businessId) && empty($accountName)) {
            throw new InvalidArgumentException('Missing parameter.');
        }
        if ($businessId) {
            $params = [
                'business_id' => $businessId,
            ];
        } else {
            $params = [
                'account_name' => $accountName,
            ];
        }

        return $this->httpPostJson('cgi-bin/business/get', $params);
    }

    /**
     * Get business list
     * @param int $offset
     * @param int $count
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function list(int $offset = 0, int $count = 10)
    {
        $params = [
            'offset' => $offset,
            'count' => $count,
        ];

        return $this->httpPostJson('cgi-bin/business/list', $params);
    }

    /**
     * Update business.
     * @param int $businessId
     * @param string $nickname
     * @param string $iconMediaId
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function update(int $businessId, string $nickname = '', string $iconMediaId = '')
    {
        $params = [
            'business_id' => $businessId,
            'nickname' => $nickname,
            'icon_media_id' => $iconMediaId,
        ];

        return $this->httpPostJson('cgi-bin/business/update', $params);
    }

    /**
     * Get message builder.
     * @param $message
     * @return Messenger
     */
    public function message($message)
    {
        $messageBuilder = new Messenger($this);

        return $messageBuilder->message($message);
    }

    /**
     * Send a message.
     * @param array $message
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function send(array $message)
    {
        return $this->httpPostJson('cgi-bin/message/custom/business/send', $message);
    }

    /**
     * Typing status.
     * @param int $businessId
     * @param string $toUser
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function typing(int $businessId, string $toUser)
    {
        $params = [
            'business_id' => $businessId,
            'touser' => $toUser,
            'command' => 'Typing',
        ];

        return $this->httpPostJson('cgi-bin/business/typing', $params);
    }
}
