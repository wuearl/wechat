<?php

namespace Wechat\MiniProgram\Shop\Spu;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * 自定义版交易组件及开放接口 - SPU接口
 *
 * @package Wechat\MiniProgram\Shop\Spu
 * @author HaoLiang <haoliang@qiyuankeji.cn>
 */
class Client extends BaseClient
{
    /**
     * 添加商品
     *
     * @param array $product 商品信息
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function add(array $product)
    {
        return $this->httpPostJson('shop/spu/add', $product);
    }

    /**
     * 删除商品
     *
     * @param array $productId 商品编号信息
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function del(array $productId)
    {
        return $this->httpPostJson('shop/spu/del', $productId);
    }

    /**
     * 获取商品
     *
     * @param array $productId 商品编号信息
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function get(array $productId)
    {
        return $this->httpPostJson('shop/spu/get', $productId);
    }

    /**
     * 获取商品列表
     *
     * @param array $product 商品信息
     * @param array $page 分页信息
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function getList(array $product, array $page)
    {
        return $this->httpPostJson('shop/spu/get_list', array_merge($product, $page));
    }

    /**
     * 撤回商品审核
     *
     * @param array $productId 商品编号信息 交易组件平台内部商品ID，与out_product_id二选一
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function delAudit(array $productId)
    {
        return $this->httpPostJson('shop/spu/del_audit', $productId);
    }

    /**
     * 更新商品
     *
     * @param array $product
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function update(array $product)
    {
        return $this->httpPostJson('shop/spu/update', $product);
    }

    /**
     * 该免审更新商品
     *
     * @param array $product
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function updateWithoutAudit(array $product)
    {
        return $this->httpPostJson('shop/spu/update_without_audit', $product);
    }

    /**
     * 上架商品
     *
     * @param array $productId 商品编号数据 交易组件平台内部商品ID，与out_product_id二选一
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function listing(array $productId)
    {
        return $this->httpPostJson('shop/spu/listing', $productId);
    }

    /**
     * 下架商品
     *
     * @param array $productId
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function delisting(array $productId)
    {
        return $this->httpPostJson('shop/spu/delisting', $productId);
    }
}
