<?php

namespace Wechat\MiniProgram\Shop\Delivery;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * 自定义版交易组件及开放接口 - 物流接口
 *
 * @package Wechat\MiniProgram\Shop\Delivery
 * @author HaoLiang <haoliang@qiyuankeji.cn>
 */
class Client extends BaseClient
{
    /**
     * 获取快递公司列表
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function getCompanyList()
    {
        return $this->httpPostJson('shop/delivery/get_company_list');
    }

    /**
     * 订单发货
     *
     * @param array $order
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function send(array $order)
    {
        return $this->httpPostJson('shop/delivery/send', $order);
    }

    /**
     * 订单确认收货
     *
     * @param array $order
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function recieve(array $order)
    {
        return $this->httpPostJson('shop/delivery/recieve', $order);
    }
}
