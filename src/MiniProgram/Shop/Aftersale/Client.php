<?php

namespace Wechat\MiniProgram\Shop\Aftersale;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * 自定义版交易组件及开放接口 - 售后接口
 *
 * @package Wechat\MiniProgram\Shop\Aftersale
 * @author HaoLiang <haoliang@qiyuankeji.cn>
 */
class Client extends BaseClient
{
    /**
     * 创建售后
     * @param array $aftersale
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function add(array $aftersale)
    {
        return $this->httpPostJson('shop/ecaftersale/add', $aftersale);
    }

    /**
     * 获取订单下售后单
     * @param array $order
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function get(array $order)
    {
        return $this->httpPostJson('shop/ecaftersale/get', $order);
    }

    /**
     * 更新售后
     * @param array $order
     * @param array $aftersale
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function update(array $order, array $aftersale)
    {
        return $this->httpPostJson('shop/ecaftersale/update', array_merge($order, $aftersale));
    }
}
