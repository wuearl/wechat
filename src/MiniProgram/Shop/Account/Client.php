<?php

namespace Wechat\MiniProgram\Shop\Account;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * 自定义版交易组件及开放接口 - 商家入驻接口
 *
 * @package Wechat\MiniProgram\Shop\Account
 * @author HaoLiang <haoliang@qiyuankeji.cn>
 */
class Client extends BaseClient
{
    /**
     * 获取商家类目列表
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function getCategoryList()
    {
        return $this->httpPostJson('shop/account/get_category_list');
    }

    /**
     * 获取商家品牌列表
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function getBrandList()
    {
        return $this->httpPostJson('shop/account/get_brand_list');
    }

    /**
     * 更新商家信息
     *
     * @param array $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function updateInfo(array $param )
    {
        return $this->httpPostJson('shop/account/update_info', $param);
    }

    /**
     * 获取商家信息
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function getInfo()
    {
        return $this->httpPostJson('shop/account/get_info');
    }
}
