<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram;

use Wechat\BasicService;
use Wechat\Kernel\ServiceContainer;

/**
 * Class Application.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 *
 * @property \Wechat\MiniProgram\Auth\AccessToken           $access_token
 * @property \Wechat\MiniProgram\DataCube\Client            $data_cube
 * @property \Wechat\MiniProgram\AppCode\Client             $app_code
 * @property \Wechat\MiniProgram\Auth\Client                $auth
 * @property \Wechat\OfficialAccount\Server\Guard           $server
 * @property \Wechat\MiniProgram\Encryptor                  $encryptor
 * @property \Wechat\MiniProgram\TemplateMessage\Client     $template_message
 * @property \Wechat\OfficialAccount\CustomerService\Client $customer_service
 * @property \Wechat\MiniProgram\Plugin\Client              $plugin
 * @property \Wechat\MiniProgram\Plugin\DevClient           $plugin_dev
 * @property \Wechat\MiniProgram\UniformMessage\Client      $uniform_message
 * @property \Wechat\MiniProgram\ActivityMessage\Client     $activity_message
 * @property \Wechat\MiniProgram\Express\Client             $logistics
 * @property \Wechat\MiniProgram\NearbyPoi\Client           $nearby_poi
 * @property \Wechat\OfficialAccount\OCR\Client             $ocr
 * @property \Wechat\MiniProgram\Soter\Client               $soter
 * @property \Wechat\BasicService\Media\Client              $media
 * @property \Wechat\BasicService\ContentSecurity\Client    $content_security
 * @property \Wechat\MiniProgram\Mall\ForwardsMall          $mall
 * @property \Wechat\MiniProgram\SubscribeMessage\Client    $subscribe_message
 * @property \Wechat\MiniProgram\RealtimeLog\Client         $realtime_log
 * @property \Wechat\MiniProgram\Search\Client              $search
 * @property \Wechat\MiniProgram\Live\Client                $live
 * @property \Wechat\MiniProgram\Broadcast\Client           $broadcast
 * @property \Wechat\MiniProgram\Business\Client            $business
 * @property \Wechat\MiniProgram\UrlLink\Client             $url_link
 * @property \Wechat\MiniProgram\QrCode\Client              $qr_code
 * @property \Wechat\MiniProgram\PhoneNumber\Client         $phone_number
 * @property \Wechat\MiniProgram\ShortLink\Client           $short_link
 * @property \Wechat\MiniProgram\Shop\Register\Client       $shop_register
 * @property \Wechat\MiniProgram\Shop\Basic\Client          $shop_basic
 * @property \Wechat\MiniProgram\Shop\Account\Client        $shop_account
 * @property \Wechat\MiniProgram\Shop\Spu\Client            $shop_spu
 * @property \Wechat\MiniProgram\Shop\Order\Client          $shop_order
 * @property \Wechat\MiniProgram\Shop\Delivery\Client       $shop_delivery
 * @property \Wechat\MiniProgram\Shop\Aftersale\Client      $shop_aftersale
 * @property \Wechat\MiniProgram\Channels\Product\Client    $channels_product
 * @property \Wechat\MiniProgram\Channels\Order\Client      $channels_order
 * @property \Wechat\MiniProgram\Channels\Freight\Client    $channels_freight
 * @property \Wechat\MiniProgram\Channels\Delivery\Client   $channels_delivery
 * @property \Wechat\MiniProgram\Channels\Coupon\Client     $channels_coupon
 * @property \Wechat\MiniProgram\Channels\Category\Client   $channels_category
 * @property \Wechat\MiniProgram\Channels\Base\Client       $channels_base
 * @property \Wechat\MiniProgram\Channels\AfterSale\Client  $channels_after_sale
 * @property \Wechat\MiniProgram\Channels\Address\Client    $channels_address
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Auth\ServiceProvider::class,
        DataCube\ServiceProvider::class,
        AppCode\ServiceProvider::class,
        Server\ServiceProvider::class,
        TemplateMessage\ServiceProvider::class,
        CustomerService\ServiceProvider::class,
        UniformMessage\ServiceProvider::class,
        ActivityMessage\ServiceProvider::class,
        OpenData\ServiceProvider::class,
        Plugin\ServiceProvider::class,
        Base\ServiceProvider::class,
        Express\ServiceProvider::class,
        NearbyPoi\ServiceProvider::class,
        OCR\ServiceProvider::class,
        Soter\ServiceProvider::class,
        Mall\ServiceProvider::class,
        SubscribeMessage\ServiceProvider::class,
        RealtimeLog\ServiceProvider::class,
        Search\ServiceProvider::class,
        Live\ServiceProvider::class,
        Broadcast\ServiceProvider::class,
        // Base services
        BasicService\Media\ServiceProvider::class,
        BasicService\ContentSecurity\ServiceProvider::class,
        Business\ServiceProvider::class,

        Shop\Register\ServiceProvider::class,
        Shop\Basic\ServiceProvider::class,
        Shop\Account\ServiceProvider::class,
        Shop\Spu\ServiceProvider::class,
        Shop\Order\ServiceProvider::class,
        Shop\Delivery\ServiceProvider::class,
        Shop\Aftersale\ServiceProvider::class,
        Business\ServiceProvider::class,
        UrlScheme\ServiceProvider::class,
        UrlLink\ServiceProvider::class,
        Union\ServiceProvider::class,
        PhoneNumber\ServiceProvider::class,
        ShortLink\ServiceProvider::class,
        QrCode\ServiceProvider::class,
        Channels\Product\ServiceProvider::class,
        Channels\Order\ServiceProvider::class,
        Channels\Freight\ServiceProvider::class,
        Channels\Delivery\ServiceProvider::class,
        Channels\Coupon\ServiceProvider::class,
        Channels\Category\ServiceProvider::class,
        Channels\Base\ServiceProvider::class,
        Channels\AfterSale\ServiceProvider::class,
        Channels\Address\ServiceProvider::class,
    ];

    /**
     * Handle dynamic calls.
     *
     * @param string $method
     * @param array  $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        return $this->base->$method(...$args);
    }
}
