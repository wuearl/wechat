<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\Delivery;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 订单发货
     *
     * https://developers.weixin.qq.com/doc/channels/API/order/delivery_send.html
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function send($param)
    {
        return $this->httpPostJson('channels/ec/order/delivery/send', $param);
    }

    /**
     * 获取快递公司列表
     *
     * https://developers.weixin.qq.com/doc/channels/API/order/deliverycompanylist_get.html
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function companyList()
    {
        return $this->httpGet('channels/ec/order/deliverycompanylist/get');
    }
}
