<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\Base;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 获取店铺基本信息
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function getInfo()
    {
        return $this->httpGet('channels/ec/basics/info/get');
    }

    /**
     * 上传图片
     * @param string $imageFilePath
     * @param int $respType
     * @param int $uploadType
     * @param int $height
     * @param int $width
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function uploadImg(string $imageFilePath, int $respType = 1, int $uploadType = 0, int $height = 0, int $width = 0)
    {
        if ($uploadType == 0) {
            return $this->httpUpload('channels/ec/basics/img/upload', [
                'media' => $imageFilePath,
            ], [], [
                'resp_type' => $respType,
                'height' => $height,
                'width' => $width
            ]);
        } else {
            return $this->httpPostJson('channels/ec/basics/img/upload', [
                'img_url' => $imageFilePath
            ], [
                'resp_type' => $respType,
                'upload_type' => $uploadType
            ]);
        }
    }
}
