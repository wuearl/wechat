<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\Order;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 获取订单列表
     *
     * https://developers.weixin.qq.com/doc/channels/API/order/list_get.html
     * @param $create_time_range
     * @param int $page_size
     * @param int $status
     * @param string $openid
     * @param string $next_key
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function list($create_time_range, int $page_size = 10, int $status = 0, string $openid = '', string $next_key = '')
    {
        $param['create_time_range'] = $create_time_range;
        $param['page_size'] = $page_size;
        if ($status) {
            $param['status'] = $status;
        }
        if ($next_key) {
            $param['next_key'] = $next_key;
        }
        if ($openid) {
            $param['openid'] = $openid;
        }
        return $this->httpPostJson('channels/ec/order/list/get', $param);
    }

    /**
     * 获取订单详情
     *
     * https://developers.weixin.qq.com/doc/channels/API/order/get.html
     * @param $order_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function get($order_id)
    {
        return $this->httpPostJson('channels/ec/order/get', ['order_id' => $order_id]);
    }

    /**
     * 修改订单价格
     *
     * https://developers.weixin.qq.com/doc/channels/API/order/price_update.html
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function updatePrice($param)
    {
        return $this->httpPostJson('channels/ec/order/price/update', $param);
    }

    /**
     * 修改订单备注
     *
     * https://developers.weixin.qq.com/doc/channels/API/order/merchantnotes_update.html
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function updateRemarks($param)
    {
        return $this->httpPostJson('channels/ec/order/merchantnotes/update', $param);
    }

    /**
     * 修改订单地址
     *
     * https://developers.weixin.qq.com/doc/channels/API/order/address_update.html
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function updateAddress($param)
    {
        return $this->httpPostJson('channels/ec/order/address/update', $param);
    }
}
