<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\Category;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 获取可用的子类目详情
     * @param int $cat_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function getCategory(int $cat_id = 0)
    {
        return $this->httpPostJson('channels/ec/category/availablesoncategories/get', ['f_cat_id' => $cat_id]);
    }

    /**
     * 上传类目资质
     * 
     * @param $category_info
     * @param array $certificate
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function add($category_info, array $certificate = [])
    {
        $category_info['certificate'] = $certificate;
        return $this->httpPostJson('channels/ec/category/add', ['category_info' => $category_info]);
    }

    /**
     * 获取审核结果
     *
     * @param $audit_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function audit($audit_id)
    {
        return $this->httpPostJson('channels/ec/category/audit/get', ['audit_id' => $audit_id]);
    }
}
