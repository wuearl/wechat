<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\Address;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 获取地址列表
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/address/list.html
     * @param $offset
     * @param int $limit
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function list($offset, int $limit = 20)
    {
        return $this->httpPostJson('channels/ec/merchant/address/list', [
            'offset' => $offset,
            'limit' => $limit
        ]);
    }

    /**
     * 添加地址
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/address/add.html
     * @param $address_detail
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function add($address_detail)
    {
        return $this->httpPostJson('channels/ec/merchant/address/add', ['address_detail' => $address_detail]);
    }

    /**
     * 获取地址详情
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/address/get.html
     * @param $address_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function get($address_id)
    {
        return $this->httpPostJson('channels/ec/merchant/address/get', ['address_id' => $address_id]);
    }

    /**
     * 更新地址
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/address/update.html
     * @param $address_detail
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function update($address_detail)
    {
        return $this->httpPostJson('channels/ec/merchant/address/update', ['address_detail' => $address_detail]);
    }

    /**
     * 删除地址
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/address/delete.html
     * @param $address_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function delete($address_id)
    {
        return $this->httpPostJson('channels/ec/merchant/address/delete', ['address_id' => $address_id]);
    }
}
