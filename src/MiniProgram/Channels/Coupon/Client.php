<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\Coupon;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 获取优惠券 ID 列表
     *
     * https://developers.weixin.qq.com/doc/channels/API/coupon/get_list.html
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function list($param)
    {
        return $this->httpPostJson('channels/ec/coupon/get_list', $param);
    }

    /**
     * 获取用户优惠券 ID 列表
     * https://developers.weixin.qq.com/doc/channels/API/coupon/get_user_coupon_list.html
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function userCouponList($param)
    {
        return $this->httpPostJson('channels/ec/coupon/get_list', $param);
    }

    /**
     * 获取用户优惠券详情
     *
     * https://developers.weixin.qq.com/doc/channels/API/coupon/get_user_coupon.html
     * @param $openid
     * @param $user_coupon_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function userCoupon($openid, $user_coupon_id)
    {
        return $this->httpPostJson('channels/ec/coupon/get', [
            "user_coupon_id" => $user_coupon_id,
            "openid" => $openid,
        ]);
    }

    /**
     * 创建优惠券
     *
     * https://developers.weixin.qq.com/doc/channels/API/coupon/create.html
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function create($param)
    {
        return $this->httpPostJson('channels/ec/coupon/create', $param);
    }

    /**
     * 更新优惠券内容
     *
     * https://developers.weixin.qq.com/doc/channels/API/coupon/update.html
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function update($param)
    {
        return $this->httpPostJson('channels/ec/coupon/update', $param);
    }

    /**
     * 获取优惠券详情
     *
     * https://developers.weixin.qq.com/doc/channels/API/coupon/get.html
     * @param $coupon_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function get($coupon_id)
    {
        return $this->httpPostJson('channels/ec/coupon/get', [
            "coupon_id" => $coupon_id
        ]);
    }

    /**
     * 更新优惠券状态
     *
     * https://developers.weixin.qq.com/doc/channels/API/coupon/update_status.html
     * @param $coupon_id
     * @param $status
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function updateStatus($coupon_id, $status)
    {
        return $this->httpPostJson('channels/ec/coupon/update_status', [
            "coupon_id" => $coupon_id,
            "status" => $status
        ]);
    }
}
