<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\Product;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 获取商品列表
     *
     * @param $page_size
     * @param int $status
     * @param string $next_key
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function list($page_size, int $status = 0, string $next_key = '')
    {
        $param['page_size'] = $page_size;
        if ($status) {
            $param['status'] = $status;
        }
        if ($next_key) {
            $param['next_key'] = $next_key;
        }
        return $this->httpPostJson('channels/ec/product/list/get', $param);
    }

    /**
     * 添加商品
     *
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function add($param)
    {
        return $this->httpPostJson('channels/ec/product/add', $param);
    }

    /**
     * 更新商品
     *
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function update($param)
    {
        return $this->httpPostJson('channels/ec/product/update', $param);
    }

    /**
     * @param $product_id
     * @param int $data_type
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function get($product_id, int $data_type = 1)
    {
        return $this->httpPostJson('channels/ec/product/get', ['product_id' => $product_id, 'data_type' => $data_type]);
    }

    /**
     * 删除商品
     *
     * @param $product_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function delete($product_id)
    {
        return $this->httpPostJson('channels/ec/product/delete', ['product_id' => $product_id]);
    }

    /**
     * 上架商品
     *
     * @param $product_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function listing($product_id)
    {
        return $this->httpPostJson('channels/ec/product/listing', ['product_id' => $product_id]);
    }

    /**
     * 下架商品
     *
     * @param $product_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function delisting($product_id)
    {
        return $this->httpPostJson('channels/ec/product/delisting', ['product_id' => $product_id]);
    }

    /**
     * 撤回商品审核
     *
     * @param $product_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function cancel($product_id)
    {
        return $this->httpPostJson('channels/ec/product/audit/cancel', ['product_id' => $product_id]);
    }

    /**
     * 快速更新库存
     *
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function updateStock($param)
    {
        return $this->httpPostJson('channels/ec/product/stock/update', $param);
    }

    /**
     * 添加限时抢购任务
     *
     * @param $param
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function limitedAdd($param)
    {
        return $this->httpPostJson('channels/ec/product/limiteddiscounttask/add', $param);
    }

    /**
     * 拉取限时抢购任务列表
     * 
     * @param $page_size
     * @param int $status
     * @param string $next_key
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function limitedList($page_size, int $status = 0, string $next_key = '')
    {
        $param['page_size'] = $page_size;
        if ($status) {
            $param['status'] = $status;
        }
        if ($next_key) {
            $param['next_key'] = $next_key;
        }
        return $this->httpPostJson('channels/ec/product/limiteddiscounttask/list/get', $param);
    }

    /**
     * 停止限时抢购任务
     *
     * @param $task_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function limitedStop($task_id)
    {
        return $this->httpPostJson('channels/ec/product/limiteddiscounttask/stop', ['task_id' => $task_id]);
    }

    /**
     * 删除限时抢购任务
     *
     * @param $task_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function limitedDelete($task_id)
    {
        return $this->httpPostJson('channels/ec/product/limiteddiscounttask/delete', ['task_id' => $task_id]);
    }
}
