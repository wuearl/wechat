<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\Freight;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 获取运费模板列表
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/getfreighttemplatelist.html
     * @param $offset
     * @param int $limit
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function list($offset, int $limit = 20)
    {
        return $this->httpPostJson('channels/ec/merchant/getfreighttemplatelist', [
            'offset' => $offset,
            'limit' => $limit
        ]);
    }

    /**
     * 查询运费模版
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/getfreighttemplatedetail.html
     * @param $template_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function get($template_id)
    {
        return $this->httpPostJson('channels/ec/merchant/getfreighttemplatedetail', ['template_id' => $template_id]);
    }

    /**
     * 增加运费模版
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/addfreighttemplate.html
     * @param array $freight_template
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function add(array $freight_template)
    {
        return $this->httpPostJson('channels/ec/merchant/addfreighttemplate', ['freight_template' => $freight_template]);
    }

    /**
     * 更新运费模版
     *
     * https://developers.weixin.qq.com/doc/channels/API/merchant/updatefreighttemplate.html
     * @param array $freight_template
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function update(array $freight_template)
    {
        return $this->httpPostJson('channels/ec/merchant/updatefreighttemplate', ['freight_template' => $freight_template]);
    }
}
