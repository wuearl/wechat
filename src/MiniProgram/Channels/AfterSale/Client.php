<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Channels\AfterSale;

use GuzzleHttp\Exception\GuzzleException;
use HttpBase\Exceptions\InvalidConfigException;
use Psr\Http\Message\ResponseInterface;
use Wechat\Kernel\BaseClient;
use Wechat\Kernel\Support\Collection;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 获取售后单
     *
     * https://developers.weixin.qq.com/doc/channels/API/aftersale/getaftersaleorder.html
     * @param $after_sale_order_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function get($after_sale_order_id)
    {
        return $this->httpPostJson('channels/ec/aftersale/getaftersaleorder', ['after_sale_order_id' => $after_sale_order_id]);
    }

    /**
     * 获取售后单列表
     *
     * https://developers.weixin.qq.com/doc/channels/API/aftersale/getaftersalelist.html
     * @param $begin_create_time
     * @param $end_create_time
     * @param string $next_key
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function list($begin_create_time, $end_create_time, string $next_key = '')
    {
        return $this->httpPostJson('channels/ec/aftersale/getaftersalelist', [
            'begin_create_time' => $begin_create_time,
            'end_create_time' => $end_create_time,
            'next_key' => $next_key,
        ]);
    }

    /**
     * 同意售后
     *
     * https://developers.weixin.qq.com/doc/channels/API/aftersale/acceptapply.html
     * @param $after_sale_order_id
     * @param $address_id
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function accept($after_sale_order_id, $address_id)
    {
        return $this->httpPostJson('channels/ec/aftersale/acceptapply', [
            'after_sale_order_id' => $after_sale_order_id,
            'address_id' => $address_id
        ]);
    }

    /**
     * 拒绝售后
     *
     * https://developers.weixin.qq.com/doc/channels/API/aftersale/rejectapply.html
     * @param $after_sale_order_id
     * @param string $reject_reason
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function reject($after_sale_order_id, string $reject_reason = '')
    {
        return $this->httpPostJson('channels/ec/aftersale/rejectapply', [
            'after_sale_order_id' => $after_sale_order_id,
            'reject_reason' => $reject_reason
        ]);
    }

    /**
     * 上传退款凭证
     *
     * https://developers.weixin.qq.com/doc/channels/API/aftersale/uploadrefundcertificate.html
     * @param $after_sale_order_id
     * @param $refund_certificates
     * @param $desc
     * @return array|object|ResponseInterface|string|Collection
     * @throws GuzzleException
     * @throws InvalidConfigException
     */
    public function refundCertificate($after_sale_order_id, $refund_certificates, $desc)
    {
        return $this->httpPostJson('channels/ec/aftersale/uploadrefundcertificate', [
            'after_sale_order_id' => $after_sale_order_id,
            'refund_certificates' => $refund_certificates,
            'desc' => $desc,
        ]);
    }
}
