<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MiniProgram\Mall;

use Wechat\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class MediaClient extends BaseClient
{
    /**
     * 更新或导入媒体信息.
     *
     * @param array $params
     *
     * @return mixed
     *
     * @throws \HttpBase\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function import($params)
    {
        return $this->httpPostJson('mall/importmedia', $params);
    }
}
