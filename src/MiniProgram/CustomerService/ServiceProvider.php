<?php

namespace Wechat\MiniProgram\CustomerService;

use Wechat\OfficialAccount\CustomerService\Client;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider.
 *
 * @author overtrue <i@overtrue.me>
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['customer_service'] = function ($app) {
            return new Client($app);
        };
    }
}
