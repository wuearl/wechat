<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat;

/**
 * Class Factory.
 *
 * @method static \Wechat\Payment\Application            payment(array $config)
 * @method static \Wechat\MiniProgram\Application        miniProgram(array $config)
 * @method static \Wechat\OpenPlatform\Application       openPlatform(array $config)
 * @method static \Wechat\OfficialAccount\Application    officialAccount(array $config)
 * @method static \Wechat\BasicService\Application       basicService(array $config)
 * @method static \Wechat\Work\Application               work(array $config)
 * @method static \Wechat\OpenWork\Application           openWork(array $config)
 * @method static \Wechat\MicroMerchant\Application      microMerchant(array $config)
 * @method static \Wechat\Market\Application             market(array $config)
 */
class Factory
{
    /**
     * @param string $name
     * @param array  $config
     *
     * @return \Wechat\Kernel\ServiceContainer
     */
    public static function make($name, array $config)
    {
        $namespace = Kernel\Support\Str::studly($name);
        $application = "\\Wechat\\{$namespace}\\Application";

        return new $application($config);
    }

    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}
