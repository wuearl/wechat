<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\Market;

use Wechat\BasicService;
use Wechat\Kernel\ServiceContainer;

/**
 * Class Application.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 *
 * @property \Wechat\Market\Server\Guard               $server
 * @property \Wechat\Market\Auth\AccessToken           $access_token
 * @property \Wechat\Market\Base\Client                $base
 * @property \Wechat\Market\Encryptor                  $encryptor
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Base\ServiceProvider::class,
        Auth\ServiceProvider::class,
        Server\ServiceProvider::class
    ];

    /**
     * Handle dynamic calls.
     *
     * @param string $method
     * @param array  $args
     *
     * @return mixed
     */
    public function __call($method, $args)
    {
        return $this->base->$method(...$args);
    }
}
