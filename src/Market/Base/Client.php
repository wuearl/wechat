<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\Market\Base;

use Wechat\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Client extends BaseClient
{
    /**
     * 登陆验证
     * @param $code
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function loginAuth($code)
    {
        return $this->httpPostJson('wxa/servicemarket/service/login_auth', compact('code'));
    }

    /**
     * 获取服务用户有效期列表
     * @param $service_id
     * @param int $offset
     * @param int $limit
     * @param int $buyer_type
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function serviceBuyerList($service_id, int $offset = 0, int $limit = 10, int $buyer_type = 1)
    {
        return $this->httpPostJson('wxa/servicemarket/service/get_service_buyer_list',
            compact('service_id', 'offset', 'limit', 'buyer_type'));
    }

    /**
     * 获取服务用户有效期
     * @param $service_id
     * @param string $appid
     * @param string $openid
     * @param int $buyer_type
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function serviceBuyer($service_id, string $appid = '', string $openid = '', int $buyer_type = 1)
    {
        return $this->httpPostJson('wxa/servicemarket/service/get_service_buyer',
            compact('service_id', 'appid', 'openid', 'buyer_type'));
    }

    /**
     * 获取用户已支付订单列表
     * @param $service_id
     * @param int $offset
     * @param int $limit
     * @param string $appid
     * @param string $openid
     * @param int $buyer_type
     * @return array|object|\Psr\Http\Message\ResponseInterface|string|\Wechat\Kernel\Support\Collection
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \HttpBase\Exceptions\InvalidConfigException
     */
    public function paidOrderList($service_id, int $offset = 0, int $limit = 10, string $appid = '', string $openid = '', int $buyer_type = 1)
    {
        return $this->httpPostJson('wxa/servicemarket/get_paid_order_list',
            compact('service_id', 'appid', 'openid', 'buyer_type', 'offset', 'limit'));
    }
}
