<?php

namespace Wechat\Market;

/**
 * Class Encryptor.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class Encryptor
{
    /**
     * App id.
     *
     * @var string
     */
    protected $appId;

    /**
     * App token.
     *
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $aesKey;

    public function __construct(string $appId, string $token = null, string $aesKey = null)
    {
        $this->appId = $appId;
        $this->token = $token;
        $this->aesKey =$aesKey;
    }

    public function decrypt($content): string
    {
        $iv = substr($this->aesKey, 0, 16);
        return openssl_decrypt($content, 'AES-128-CBC', $iv, OPENSSL_CIPHER_RC2_40, $iv);
    }
}
