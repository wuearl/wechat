<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\MicroMerchant\Kernel\Exceptions;

use HttpBase\Exceptions\Exception;

/**
 * Class EncryptException.
 *
 * @author   liuml  <liumenglei0211@163.com>
 */
class EncryptException extends Exception
{
}
