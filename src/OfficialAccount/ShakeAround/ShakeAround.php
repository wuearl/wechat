<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\OfficialAccount\ShakeAround;

use HttpBase\Exceptions\InvalidArgumentException;

/**
 * Class Card.
 *
 * @author overtrue <i@overtrue.me>
 *
 * @property \Wechat\OfficialAccount\ShakeAround\DeviceClient   $device
 * @property \Wechat\OfficialAccount\ShakeAround\GroupClient    $group
 * @property \Wechat\OfficialAccount\ShakeAround\MaterialClient $material
 * @property \Wechat\OfficialAccount\ShakeAround\RelationClient $relation
 * @property \Wechat\OfficialAccount\ShakeAround\StatsClient    $stats
 */
class ShakeAround extends Client
{
    /**
     * @param string $property
     *
     * @return mixed
     *
     * @throws \HttpBase\Exceptions\InvalidArgumentException
     */
    public function __get($property)
    {
        if (isset($this->app["shake_around.{$property}"])) {
            return $this->app["shake_around.{$property}"];
        }

        throw new InvalidArgumentException(sprintf('No shake_around service named "%s".', $property));
    }
}
