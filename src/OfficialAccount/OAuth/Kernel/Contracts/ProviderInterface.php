<?php

/*
 * This file is part of the overtrue/socialite.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\OfficialAccount\OAuth\Kernel\Contracts;

interface ProviderInterface
{
    /**
     * Redirect the user to the authentication page for the provider.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect();

    /**
     * Get the User instance for the authenticated user.
     *
     * @param \Wechat\OfficialAccount\OAuth\Kernel\Contracts\AccessTokenInterface $token
     *
     * @return \Overtrue\Socialite\User
     */
    public function user(AccessTokenInterface $token = null);
}
