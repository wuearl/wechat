<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\OfficialAccount;

use Wechat\BasicService;
use Wechat\Kernel\ServiceContainer;

/**
 * Class Application.
 *
 * @author overtrue <i@overtrue.me>
 *
 * @property \Wechat\BasicService\Media\Client                     $media
 * @property \Wechat\BasicService\Url\Client                       $url
 * @property \Wechat\BasicService\QrCode\Client                    $qrcode
 * @property \Wechat\BasicService\Jssdk\Client                     $jssdk
 * @property \Wechat\OfficialAccount\Auth\AccessToken              $access_token
 * @property \Wechat\OfficialAccount\Server\Guard                  $server
 * @property \Wechat\OfficialAccount\User\UserClient               $user
 * @property \Wechat\OfficialAccount\User\TagClient                $user_tag
 * @property \Wechat\OfficialAccount\Menu\Client                   $menu
 * @property \Wechat\OfficialAccount\TemplateMessage\Client        $template_message
 * @property \Wechat\OfficialAccount\Material\Client               $material
 * @property \Wechat\OfficialAccount\CustomerService\Client        $customer_service
 * @property \Wechat\OfficialAccount\CustomerService\SessionClient $customer_service_session
 * @property \Wechat\OfficialAccount\Semantic\Client               $semantic
 * @property \Wechat\OfficialAccount\DataCube\Client               $data_cube
 * @property \Wechat\OfficialAccount\AutoReply\Client              $auto_reply
 * @property \Wechat\OfficialAccount\Broadcasting\Client           $broadcasting
 * @property \Wechat\OfficialAccount\Card\Card                     $card
 * @property \Wechat\OfficialAccount\Device\Client                 $device
 * @property \Wechat\OfficialAccount\ShakeAround\ShakeAround       $shake_around
 * @property \Wechat\OfficialAccount\POI\Client                    $poi
 * @property \Wechat\OfficialAccount\Store\Client                  $store
 * @property \Wechat\OfficialAccount\Base\Client                   $base
 * @property \Wechat\OfficialAccount\Comment\Client                $comment
 * @property \Wechat\OfficialAccount\OCR\Client                    $ocr
 * @property \Wechat\OfficialAccount\Goods\Client                  $goods
 * @property \Wechat\OfficialAccount\OAuth\Kernel\Providers\WeChatServiceProvider              $oauth
 * @property \Wechat\OfficialAccount\WiFi\Client                   $wifi
 * @property \Wechat\OfficialAccount\WiFi\CardClient               $wifi_card
 * @property \Wechat\OfficialAccount\WiFi\DeviceClient             $wifi_device
 * @property \Wechat\OfficialAccount\WiFi\ShopClient               $wifi_shop
 * @property \Wechat\OfficialAccount\FreePublish\Client            $free_publish
 * @property \Wechat\OfficialAccount\Guide\Client                  $guide
 * @property \Wechat\OfficialAccount\Draft\Client                  $draft
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Auth\ServiceProvider::class,
        Server\ServiceProvider::class,
        User\ServiceProvider::class,
        OAuth\ServiceProvider::class,
        Menu\ServiceProvider::class,
        TemplateMessage\ServiceProvider::class,
        Material\ServiceProvider::class,
        CustomerService\ServiceProvider::class,
        Semantic\ServiceProvider::class,
        DataCube\ServiceProvider::class,
        POI\ServiceProvider::class,
        AutoReply\ServiceProvider::class,
        Broadcasting\ServiceProvider::class,
        Card\ServiceProvider::class,
        Device\ServiceProvider::class,
        ShakeAround\ServiceProvider::class,
        Store\ServiceProvider::class,
        Comment\ServiceProvider::class,
        Base\ServiceProvider::class,
        OCR\ServiceProvider::class,
        Goods\ServiceProvider::class,
        WiFi\ServiceProvider::class,
        FreePublish\ServiceProvider::class,
        // Base services
        BasicService\QrCode\ServiceProvider::class,
        BasicService\Media\ServiceProvider::class,
        BasicService\Url\ServiceProvider::class,
        BasicService\Jssdk\ServiceProvider::class,
        Guide\ServiceProvider::class,
        Draft\ServiceProvider::class
    ];
}
