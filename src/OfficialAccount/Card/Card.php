<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\OfficialAccount\Card;

use HttpBase\Exceptions\InvalidArgumentException;

/**
 * Class Card.
 *
 * @author overtrue <i@overtrue.me>
 *
 * @property \Wechat\OfficialAccount\Card\CodeClient          $code
 * @property \Wechat\OfficialAccount\Card\MeetingTicketClient $meeting_ticket
 * @property \Wechat\OfficialAccount\Card\MemberCardClient    $member_card
 * @property \Wechat\OfficialAccount\Card\GeneralCardClient   $general_card
 * @property \Wechat\OfficialAccount\Card\MovieTicketClient   $movie_ticket
 * @property \Wechat\OfficialAccount\Card\CoinClient          $coin
 * @property \Wechat\OfficialAccount\Card\SubMerchantClient   $sub_merchant
 * @property \Wechat\OfficialAccount\Card\BoardingPassClient  $boarding_pass
 * @property \Wechat\OfficialAccount\Card\JssdkClient         $jssdk
 * @property \Wechat\OfficialAccount\Card\GiftCardClient      $gift_card
 * @property \Wechat\OfficialAccount\Card\GiftCardOrderClient $gift_card_order
 * @property \Wechat\OfficialAccount\Card\GiftCardPageClient  $gift_card_page
 * @property \Wechat\OfficialAccount\Card\InvoiceClient       $invoice
 */
class Card extends Client
{
    /**
     * @param string $property
     *
     * @return mixed
     *
     * @throws \HttpBase\Exceptions\InvalidArgumentException
     */
    public function __get($property)
    {
        if (isset($this->app["card.{$property}"])) {
            return $this->app["card.{$property}"];
        }

        throw new InvalidArgumentException(sprintf('No card service named "%s".', $property));
    }
}
