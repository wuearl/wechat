<?php

namespace Wechat\Kernel\Events;

use Wechat\Kernel\ServiceContainer;

/**
 * Class ApplicationInitialized.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class ApplicationInitialized
{
    /**
     * @var \Wechat\Kernel\ServiceContainer
     */
    public $app;

    /**
     * @param \Wechat\Kernel\ServiceContainer $app
     */
    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
    }
}
