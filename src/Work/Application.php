<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Wechat\Work;

use Wechat\Kernel\ServiceContainer;
use Wechat\Work\MiniProgram\Application as MiniProgram;

/**
 * Application.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 *
 * @property \Wechat\Work\OA\Client                        $oa
 * @property \Wechat\Work\Auth\AccessToken                 $access_token
 * @property \Wechat\Work\Agent\Client                     $agent
 * @property \Wechat\Work\Department\Client                $department
 * @property \Wechat\Work\Media\Client                     $media
 * @property \Wechat\Work\Menu\Client                      $menu
 * @property \Wechat\Work\Message\Client                   $message
 * @property \Wechat\Work\Message\Messenger                $messenger
 * @property \Wechat\Work\User\Client                      $user
 * @property \Wechat\Work\User\TagClient                   $tag
 * @property \Wechat\Work\Server\Guard                     $server
 * @property \Wechat\Work\Jssdk\Client                     $jssdk
 * @property \Wechat\Work\OAuth\Kernel\Providers\WeWorkServiceProvider      $oauth
 * @property \Wechat\Work\Invoice\Client                   $invoice
 * @property \Wechat\Work\Chat\Client                      $chat
 * @property \Wechat\Work\ExternalContact\Client           $external_contact
 * @property \Wechat\Work\ExternalContact\ContactWayClient $contact_way
 * @property \Wechat\Work\ExternalContact\StatisticsClient $external_contact_statistics
 * @property \Wechat\Work\ExternalContact\MessageClient    $external_contact_message
 * @property \Wechat\Work\GroupRobot\Client                $group_robot
 * @property \Wechat\Work\GroupRobot\Messenger             $group_robot_messenger
 * @property \Wechat\Work\Calendar\Client                  $calendar
 * @property \Wechat\Work\Schedule\Client                  $schedule
 *
 * @method mixed getCallbackIp()
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        OA\ServiceProvider::class,
        Auth\ServiceProvider::class,
        Base\ServiceProvider::class,
        Menu\ServiceProvider::class,
        OAuth\ServiceProvider::class,
        User\ServiceProvider::class,
        Agent\ServiceProvider::class,
        Media\ServiceProvider::class,
        Message\ServiceProvider::class,
        Department\ServiceProvider::class,
        Server\ServiceProvider::class,
        Jssdk\ServiceProvider::class,
        Invoice\ServiceProvider::class,
        Chat\ServiceProvider::class,
        ExternalContact\ServiceProvider::class,
        GroupRobot\ServiceProvider::class,
        Calendar\ServiceProvider::class,
        Schedule\ServiceProvider::class,
    ];

    /**
     * @var array
     */
    protected $defaultConfig = [
        // http://docs.guzzlephp.org/en/stable/request-options.html
        'http' => [
            'base_uri' => 'https://qyapi.weixin.qq.com/',
        ],
    ];

    /**
     * Creates the miniProgram application.
     *
     * @return \Wechat\Work\MiniProgram\Application
     */
    public function miniProgram(): MiniProgram
    {
        return new MiniProgram($this->getConfig());
    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        return $this['base']->$method(...$arguments);
    }
}
